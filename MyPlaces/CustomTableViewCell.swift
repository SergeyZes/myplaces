//
//  CustomTableViewCell.swift
//  MyPlaces
//
//  Created by Сергей Зесли on 30/09/2019.
//  Copyright © 2019 Сергей Зесли. All rights reserved.
//

import UIKit
import Cosmos

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var imageOfPlace: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var cosmosView: CosmosView!
    

}
